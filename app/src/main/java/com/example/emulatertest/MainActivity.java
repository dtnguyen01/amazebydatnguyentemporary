package com.example.emulatertest;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.content.Intent;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    //variables to be passed to GeneratingActivity.java to generate maze
    public static final String EXTRA_MODE = "com.example.emulatertest.EXTRA_MODE";
    public static final String EXTRA_SIZE = "com.example.emulatertest.EXTRA_SIZE";
    public static final String EXTRA_ALGORITHM = "com.example.emulatertest.EXTRA_ALGORITHM";
    public static final String EXTRA_ROOMS = "com.example.emulatertest.EXTRA_ROOMS";
    public static final String EXTRA_MAPMODE = "com.example.emulatertest.EXTRA_MAPMODE";

    //(Widget variables)
    SeekBar sizeSB;
    TextView sizevalueT;
    Spinner algorithmSpinner;
    Spinner playmodeSpinner;

    private Button btnRVST;
    private Button btnEXP;

    Spinner Manual;
    Spinner AutoMode;

    private Switch RoomSwitch;
    //(Widget variables)

     //(Maze Values)
    String PlayingMode = "Manual";
    String Algorithm = "DFS";
    int Maze_Size = 0;
    boolean Rooms;
    String MapMode;
    //(Maze Values)



    //test1
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sizeSB = findViewById(R.id.SizeSeekBar);
        sizevalueT = findViewById(R.id.SizeValue);


        //Switch to determine if maze should have rooms
        RoomSwitch = findViewById(R.id.roomSwitch);
        RoomSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Rooms = true;
                }
            }
        });

        //Spinner for selecting algorithm (Values = DFS, Prim, Kruskal)
        algorithmSpinner = findViewById(R.id.MazeGenerationSpinner);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.algorithms, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_item);
        algorithmSpinner.setAdapter(adapter1);
        algorithmSpinner.setOnItemSelectedListener(this);


        //Spinner for selecting playing mode (Values = Manual, WallFollower, Wizard)
        playmodeSpinner = findViewById(R.id.PlayingModeSpinner);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.playingmodes, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        playmodeSpinner.setAdapter(adapter2);
        playmodeSpinner.setOnItemSelectedListener(this);


        //SeekBar to determine size/difficulty of maze (Values = 0-9)
        sizeSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                sizevalueT.setText(String.valueOf(progress));
                Maze_Size = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //Button for determining if maze should be selected from previous mazes or generated new
        btnRVST = findViewById(R.id.btnRevisit);
        btnEXP = findViewById(R.id.btnExplore);

        btnRVST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapMode = "Revisit";
                openGeneratingActivity();

            }
        });

        btnEXP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapMode = "Explore";
                openGeneratingActivity();



            }
        });

    }





    //switch to GeneratingActivity.java (where loading screen is and maze will be built)
    public void openGeneratingActivity() {
        Intent SwitchToGenerating = new Intent(this, GeneratingActivity.class);
        SwitchToGenerating.putExtra(EXTRA_SIZE, Maze_Size);
        SwitchToGenerating.putExtra(EXTRA_MODE, PlayingMode);
        SwitchToGenerating.putExtra(EXTRA_ALGORITHM, Algorithm);
        SwitchToGenerating.putExtra(EXTRA_ROOMS, Rooms);
        SwitchToGenerating.putExtra(EXTRA_MAPMODE, MapMode);
        startActivity(SwitchToGenerating);
    }





    //Receiving values from spinners
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


        if (parent.getId() == R.id.PlayingModeSpinner){
            PlayingMode = parent.getItemAtPosition(position).toString();
        }
        else if (parent.getId() == R.id.MazeGenerationSpinner) {
            Algorithm = parent.getItemAtPosition(position).toString();
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}