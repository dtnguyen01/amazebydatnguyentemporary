package com.example.emulatertest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class GeneratingActivity extends AppCompatActivity {
    //loading bar variables
    ProgressBar tempBar;
    int counter = 0;
    TextView tempBarStr;


    //testing variables to make sure all values are passed from MainActivity.java correctly
    TextView sizeText;
    TextView roomText;
    TextView algorithmText;
    TextView playingmodeText;
    TextView explorationmodeText;

    boolean doneloading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating);

        //receives variables from MainActivity.java
        Intent intent = getIntent();
        String Playing_Mode = intent.getStringExtra(MainActivity.EXTRA_MODE);
        int Maze_Size = intent.getIntExtra(MainActivity.EXTRA_SIZE, 0);
        String Algorithm = intent.getStringExtra(MainActivity.EXTRA_ALGORITHM);
        boolean Rooms = intent.getBooleanExtra(MainActivity.EXTRA_ROOMS, false);
        String MapMode = intent.getStringExtra(MainActivity.EXTRA_MAPMODE);



        //edit TextView variables to show selection on screen
        sizeText = findViewById(R.id.SizeText);
        String test = Integer.toString(Maze_Size);
        sizeText.setText(test);

        roomText = findViewById(R.id.RoomText);
        if (Rooms == true) {
            roomText.setText("true");
        }
        else{
            roomText.setText("false");
        }

        algorithmText = findViewById(R.id.AlgorithmText);
        algorithmText.setText(Algorithm);

        playingmodeText = findViewById(R.id.PlayingModeText);
        playingmodeText.setText(Playing_Mode);

        explorationmodeText = findViewById(R.id.ExplorationModeText);
        explorationmodeText.setText(MapMode);



        //run loading bar
        runProgress(Playing_Mode);



    }


    //Method to start and show loading bar
    public void runProgress(String text){

        tempBar = findViewById(R.id.progressBar);
        tempBarStr = findViewById(R.id.progressBarStr);
        Timer t = new Timer();
        TimerTask tt = new TimerTask() {
            @Override
            public void run() {
                counter ++;
                tempBar.setProgress(counter);
                if (counter == 100){
                    t.cancel();

                    if (text.equals("Manual")){
                        openPlayManuallyActivity();
                    }

                    else if (text.equals("WallFollower") || text.equals("Wizard")){
                        openPlayAnimationActivity();
                    }




                }
            }
        };
        t.schedule(tt, 0, 100);



    }

    public void openPlayManuallyActivity() {
        Intent SwitchToPlaying = new Intent(this, PlayManuallyActivity.class);
        startActivity(SwitchToPlaying);
    }

    public void openPlayAnimationActivity(){
        Intent SwitchToPlaying = new Intent(this, PlayAnimationActivity.class);
        startActivity(SwitchToPlaying);
    }

}