package com.example.emulatertest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FinishActivityManual extends AppCompatActivity {
    Button Resetbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_manual);

        Resetbtn = findViewById(R.id.resetButton);

        Resetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivity();
            }
        });
    }


    public void openMainActivity(){
        Intent SwitchToMain = new Intent(this, MainActivity.class);
        startActivity(SwitchToMain);
    }
}