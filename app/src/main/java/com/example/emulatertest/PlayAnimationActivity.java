package com.example.emulatertest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PlayAnimationActivity extends AppCompatActivity {
    Button WinShortcut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_animation);

        WinShortcut = findViewById(R.id.FinishShortcut);
        WinShortcut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFinishActivityAnimationWin();
            }
        });
    }

    public void openFinishActivityAnimationWin(){
        Intent SwitchToWinning = new Intent(this, FinishActivityAnimationWin.class);
        startActivity(SwitchToWinning);
    }
}