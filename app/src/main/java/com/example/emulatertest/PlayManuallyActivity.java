package com.example.emulatertest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PlayManuallyActivity extends AppCompatActivity {
    Button WinShortcut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually);

        WinShortcut = findViewById(R.id.FinishShortcut);
        WinShortcut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFinishActivityManual();
            }
        });
    }

    public void openFinishActivityManual(){
        Intent SwitchToFinish = new Intent(this, FinishActivityManual.class);
        startActivity(SwitchToFinish);
    }
}